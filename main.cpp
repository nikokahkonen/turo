#include <QDebug>
#include "wsserverapplication.h"

int main(int argc, char *argv[])
{
    qDebug() << Q_FUNC_INFO;
    WSServerApplication a(argc, argv);
    if (a.isRoot()) {
        qWarning() << "Not allowed to run wsserver as root!";
        return -1;
    }
    a.disableCoreDump();
    a.startServer(1234);
    return a.exec();
}
