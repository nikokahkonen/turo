#ifndef CREDENTIALMANAGER_H
#define CREDENTIALMANAGER_H

#include <QByteArray>
#include <QCryptographicHash>
#include <QObject>
#include <QString>

class CredentialManager : public QObject
{
    Q_OBJECT

public:
    static CredentialManager &instance();
    void init();
    bool setPassword(QString const& newPassword);
    bool hasAccess(QString const& password);
    bool checkPasswordQuality(QString const& password);
    bool isPasswordSet() const;

signals:
    void passwordSet();

private:
    CredentialManager();
    QByteArray createSalt();
    QByteArray createHash(const QByteArray &salt, QString const& password);
};

#endif // CREDENTIALMANAGER_H
