# WSServer (Weather Station Server)
## General
WSServer implements a wather station server, which monitors values from weather station and stores them to local database. User is able to configure the station and define for example the monitoring sequence and the values to be monitoring. User is also able to request current values or the weather history data, and request current weather data.

For configuration and weather data requests, WSServer implements HTTP REST API.

WSServer gets weather data from weather sensor module also via HTTP REST API. This API is offered by the weather sensor module and is out of scope of this application.

## Security considerations
### User management
 - All configuration functionality is protected by administrator password.
 - Passwords are never stored to database or file as plain text. Hashes are used instead of.
 - After 5 sequential failed administrator login attempts, administrator account is locked for 15 minutes. After the next failed 5 attempts, the administrator account is locked permanently. Administrator account can be reset to default using account reset button on weather station device.
 - After administator account reset, it has a default password. However, the server cannot be configured to used before user has changed the default password.
 - All data viewing requests are publicly available, with no password. This application stores and displays only weather data, which is not secret or confidential.
### Countermeasures for known attack types and vulnerabilities
#### Directory travelsal attack
 - HTTP argument sanitation
 - server is run as a regular user with minimal access rights
 - no direct access to files from public interface (data is read from cache)
#### Command injection
 - HTTP argument sanitation
 - never use arguments directly as part of system call arguments
 - restricted sandbox environment, only needed commands available on server system
 - server is run as a regular user, with minimal access rights
#### SQL injection
 - HTTP argument sanitations
 - never use arguments directly as part of SQL command, not even partially
#### Denial of service
 - restricted operations per second from single ip address
#### Buffer overflow vulnerabilities
 - use safe buffer/string classes of framework (QString, QByteArray)
 - even though direct QByteArray/QString manipulation for example with memcpy would be possible, DO NOT DO IT! Use only safe QString/QByteArray operations
 - use smart pointers instead of direct pointers
   - helps to avoid usage of dangling pointers (pointer to uninitialized/deleted memory)
 - do static code analysis
#### Temporary and log files
 - to keep passwords and other sensitive information secured, application does not generate core dump files 
 - temporary file access is only for application user
 - temporary files are not generated to /tmp/ or any other common location
 - temporary files uses random non-predictable names
