#include <algorithm>
#include <QJsonDocument>
#include <QJsonObject>
#include "credentialmanager.h"
#include "wsrequesthandlerconfig.h"
#include "dabasehandler.h"

WSRequestHandlerConfig::WSRequestHandlerConfig(QObject* parent)
    : WSRequestHandler(parent)
{
}

void WSRequestHandlerConfig::run(const QByteArray &data, QTcpSocket *socket)
{
    // TODO: Credentialmanager and password part is similar as in WSRequestHandlerSetPassword!
    // Should be one common implementation for all PUT/POST methods
    QJsonDocument doc = QJsonDocument::fromJson(data);
    QJsonObject root = doc.object();
    QString password = root.value("password").toString();
    if (!CredentialManager::instance().hasAccess(password))
    {
        WSRequestHandler::sendResponse(403, QByteArray(), socket);
        return;
    }

    int frequence = root.value("save_frequence").toInt(); // how many times in day to save the data?
    frequence = std::min(std::max(frequence, 0), 24*60); // clamp between 0...24*60
    DabaseHandler::instance().setFrequence(frequence); // save to database

    WSRequestHandler::sendResponse(200, QByteArray(), socket);
}
