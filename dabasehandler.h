#ifndef DABASEHANDLER_H
#define DABASEHANDLER_H

#include <QSqlDatabase>

class DabaseHandler
{
public:
    static DabaseHandler &instance();
    void open();
    void createTables();
    void setFrequence(int frequence);

    bool setCredentials(QString username, QByteArray hash, QByteArray salt);
    bool credentials(QString username, QByteArray& hash, QByteArray& salt);
    int frequence() const;

private:
    DabaseHandler();

private:
    QSqlDatabase m_db;
    int m_frequence;
};

#endif // DABASEHANDLER_H
