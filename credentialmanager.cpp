#include <QCryptographicHash>
#include <QDateTime>
#include <QRegularExpression>
#include <QThread>
#include "credentialmanager.h"
#include "dabasehandler.h"

static const QCryptographicHash::Algorithm ALGORITHM{QCryptographicHash::Algorithm::Sha512};
static const int PASSWORD_MIN_LENGTH{10};
static const QString USERNAME{"admin"};

CredentialManager::CredentialManager()
{
}

CredentialManager &CredentialManager::instance()
{
    static CredentialManager *cmi = nullptr;
    if (nullptr == cmi) {
        cmi = new CredentialManager();
    }
    return *cmi;
}

void CredentialManager::init()
{
    if (isPasswordSet()) {
        emit passwordSet();
    }
}

bool CredentialManager::setPassword(const QString &newPassword)
{
    QByteArray salt = createSalt();
    QByteArray hash = createHash(salt, newPassword);
    DabaseHandler::instance().setCredentials(USERNAME, hash, salt);
    bool succeeded = isPasswordSet();
    if (succeeded) {
        emit passwordSet();
    }
    return succeeded;
}

bool CredentialManager::hasAccess(const QString &password)
{
    QByteArray hashFromDb;
    QByteArray salt;
    if (DabaseHandler::instance().credentials(USERNAME, hashFromDb, salt)) {
        QByteArray hash = QCryptographicHash::hash(salt + password.toUtf8(), ALGORITHM);
        return (hash == hashFromDb);
    }
    return true;
}

/*
 * Creates salt to be used with password. Security considerations:
 *  1. Salt must be long enough. Good rule is to use as long salt as the password hash.
 *     So in this case we use 512 bytes, since we use Sha512 in password hashing
 *  2. Salt must be non-predictable and unique for all users. So we don't use username
 *     or any constant for the hash, but we calculate salt from current date & time
 */
QByteArray CredentialManager::createSalt()
{
    QByteArray time(QString("%1").arg(QDateTime::currentDateTime().toSecsSinceEpoch()).toUtf8());
    return QCryptographicHash::hash(time, ALGORITHM);
}

QByteArray CredentialManager::createHash(const QByteArray &salt, const QString &password)
{
    return QCryptographicHash::hash(salt + password.toUtf8(), ALGORITHM);
}

bool CredentialManager::checkPasswordQuality(const QString &password)
{
    if (password.length() < PASSWORD_MIN_LENGTH)
        return false;
    // Make sure to avoid complex regexps!
    // Complex regexp is potential performance problem, which would make us vulnere to DOS attack
    if (!password.contains(QRegularExpression("\\d+")))
        return false;
    if (!password.contains(QRegularExpression("[a-z]+")))
        return false;
    if (!password.contains(QRegularExpression("[A-Z]+")))
        return false;
    return true;
}

bool CredentialManager::isPasswordSet() const
{
    QByteArray hash;
    QByteArray salt;
    DabaseHandler::instance().credentials(USERNAME, hash, salt);
    return !hash.isEmpty();
}
