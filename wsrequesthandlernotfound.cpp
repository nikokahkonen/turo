#include "wsrequesthandlernotfound.h"

WSRequestHandlerNotFound::WSRequestHandlerNotFound(QObject *parent)
    : WSRequestHandler(parent)
{
}

void WSRequestHandlerNotFound::run(const QByteArray &data, QTcpSocket *socket)
{
    Q_UNUSED(data);
    WSRequestHandler::sendResponse(404, QByteArray("404"), socket);
}
