#include <QDebug>
#include <QDir>
#include <QHostAddress>
#include <QStandardPaths>
#include "wsserverapplication.h"
#include "wshttpserver.h"
#include "credentialmanager.h"
#include "wsrequesthandlerweather.h"
#include "wsrequesthandlersetpassword.h"
#include "wsrequesthandlerconfig.h"

#ifdef Q_LINUX
#include <unistd.h>
#include <sys/types.h>
#endif

WSServerApplication::WSServerApplication(int argc, char *argv[])
    : QCoreApplication(argc, argv)
{
    qDebug() << Q_FUNC_INFO;
    // add setPassword route always, the rest only if password is already set
    m_restServer.addHandler(WSHttpServer::RequestType::PUT,
                            "/password",
                            new WSRequestHandlerSetPassword(this));
    // add only setPassword route if
    CredentialManager &credentialManager = CredentialManager::instance();
    // connect to signal and add routes after password is succesfully set
    connect(&credentialManager, &CredentialManager::passwordSet,
            this, &WSServerApplication::addRoutes);
    credentialManager.init();
}

void WSServerApplication::disableCoreDump()
{
#ifdef Q_LINUX
    rlimit limits;
    limits.rlim_cur = 0;
    limits.rlim_max = 0;
    setrlimit(RLIMIT_CORE, &limits);
#endif
}

void WSServerApplication::startServer(quint16 port)
{
    m_restServer.listen(QHostAddress::Any, port);
}

QString WSServerApplication::databaseFileLocation()
{
    QStringList homes = QStandardPaths::standardLocations(QStandardPaths::HomeLocation);
    Q_ASSERT(!homes.isEmpty());
    if (homes.isEmpty()) {
        // qFatal() exits the application
        qFatal("Failed to determine home directory!");
    }
    QDir settingsDir(homes.first() + "/.wsserver");
    if (!settingsDir.exists()) {
        QDir().mkpath(settingsDir.path());
    }
    return settingsDir.path();
}

bool WSServerApplication::isRoot() const
{
#ifdef Q_LINUX
    return (getuid() != 0 && geteuid() != 0);
#else
    return false;
#endif
}

void WSServerApplication::addRoutes()
{
    // to make sure routes are added only once, after setting password
    disconnect(&CredentialManager::instance(), &CredentialManager::passwordSet,
               this, &WSServerApplication::addRoutes);
    // add routes
    m_restServer.addHandler(WSHttpServer::RequestType::GET,
                            "/weather",
                            new WSRequestHandlerWeather(this));
    m_restServer.addHandler(WSHttpServer::RequestType::PUT,
                            "/config",
                            new WSRequestHandlerConfig(this));
}
