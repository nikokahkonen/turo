#include <QDateTime>
#include "wsrequesthandler.h"

WSRequestHandler::WSRequestHandler(QObject *parent)
    : QObject(parent)
{
}

void WSRequestHandler::sendResponse(int responseCode,
                                    const QByteArray &data,
                                    QTcpSocket *socket)
{
    Q_ASSERT(socket != nullptr);
    if (socket == nullptr)
        return;

    QString header = QString("HTTP/1.1 %1\r\n" \
                      "Content-type: text/html; charset=utf-8\r\n" \
                      "Content-Length: %2\r\n" \
                      "Cache-Control: max-age=0\r\n" \
                      "Connection: close\r\n" \
                      "\r\n").arg(responseFromCode(responseCode)).arg(data.length());
    socket->write(header.toUtf8() + data);
    socket->close();
}

QByteArray WSRequestHandler::errorJson(QString errorCode)
{
    return QString("{\"error\": \"%1\"}").arg(errorCode).toUtf8();
}

QString WSRequestHandler::responseFromCode(int responseCode)
{
    switch(responseCode)
    {
      case 200:
        return QString("200 OK");
      case 201:
        return QString("201 Created");
      case 204:
        return QString("204 No Content");
      case 403:
        return QString("403 Forbidden");
      case 404:
        return QString("404 Not Found");
      case 500:
        return QString("500 Internal Server Error");
      default:
        return QString("500 Internal Server Error");
    }
}
