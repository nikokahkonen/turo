#include <QDateTime>
#include "wsrequesthandlerweather.h"

WSRequestHandlerWeather::WSRequestHandlerWeather(QObject* parent)
    : WSRequestHandler(parent),
      m_temperature(0),
      m_dewpoint(0)
{
    // test data
    m_temperature = 14;
    m_dewpoint = 11;
}

void WSRequestHandlerWeather::run(QByteArray const& data, QTcpSocket* socket)
{
    Q_UNUSED(data);
    Q_ASSERT(socket != nullptr);
    if (socket == nullptr)
        return;

    QString resp = QString("{\"time\": \"%1\",\"temperature\": %2, \"dewpoint\": %3}")
                           .arg(QDateTime::currentDateTime().toString(Qt::DateFormat::ISODate))
                           .arg(m_temperature)
                           .arg(m_dewpoint);
    WSRequestHandler::sendResponse(200, resp.toUtf8(), socket);
}
