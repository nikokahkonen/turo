#include <QFile>
#include <QtGlobal>
#include <QSslCertificate>
#include <QSslKey>
#include <QSslSocket>
#include "wshttpserver.h"
#include "wsrequesthandler.h"
#include "wsrequesthandlernotfound.h"

static const QString KEY_404("404");

QString TypeToString(WSHttpServer::RequestType type)
{
    switch(type) {
      case WSHttpServer::RequestType::GET:
        return "GET";
      case WSHttpServer::RequestType::PUT:
        return "PUT";
      case WSHttpServer::RequestType::POST:
        return "POST";
    }
    return QString();
}

WSHttpServer::WSHttpServer(QObject *parent)
    : QObject(parent),
      m_sslServer()
{
    m_handlers[KEY_404] = new WSRequestHandlerNotFound(this);

    QString openSslRuntimeVersion = QSslSocket::sslLibraryVersionString();
    QString openSslBuildVersion = QSslSocket::sslLibraryBuildVersionString();
    qDebug() << "OpenSsl runtime version:" << openSslRuntimeVersion;
    qDebug() << "OpenSsl Qt build version:" << openSslBuildVersion;
    Q_ASSERT(!openSslRuntimeVersion.isEmpty());
}

void WSHttpServer::listen(QHostAddress host, quint16 port)
{
    // connect signals
    connect(&m_sslServer, &QSslServer::newEncryptedConnection,
            this, &WSHttpServer::handleNewConnection);
    connect(&m_sslServer, &QSslServer::sslErrors,
            this, &WSHttpServer::onSslErrors);
    // set ssl configuration
    QFile certFile(":/ssl/wsserver.pem");
    bool open = certFile.open(QIODevice::ReadOnly);
    Q_ASSERT(open == true);
    QByteArray certData = certFile.readAll();
    QSslCertificate cert(certData);
    QFile keyFile(":/ssl/wsserver.key");
    open = keyFile.open(QIODevice::ReadOnly);
    QByteArray keyData = keyFile.readAll();
    QSslKey key(keyData, QSsl::Rsa);
    QSslConfiguration config = m_sslServer.sslConfiguration();
    config.setLocalCertificate(cert);
    config.setPrivateKey(key);
    m_sslServer.setSslConfiguration(config);
    // start listening
    m_sslServer.listen(host, port);
}

void WSHttpServer::addHandler(WSHttpServer::RequestType type, const QString &url, WSRequestHandler *handler)
{
    Q_ASSERT(handler != nullptr);
    if (handler == nullptr) {
        return;
    }

    QString mapString = QString("%1 %2").arg(TypeToString(type)).arg(url);
    m_handlers[mapString] = handler;
}

void WSHttpServer::handleNewConnection()
{
    while (m_sslServer.hasPendingConnections()) {
        QSslSocket *socket = m_sslServer.nextPendingConnection();
        // todo: create new handler instance for each connection, to make sure
        // all requests are handled without problems (WSHttpServer is NOT thread safe)
        connect(socket, &QSslSocket::readyRead, this, &WSHttpServer::readDataFromSocket);
    }
}

void WSHttpServer::onSslErrors(const QList<QSslError> &errors)
{
    qDebug() << errors;
}

void WSHttpServer::readDataFromSocket()
{
    qDebug() << Q_FUNC_INFO;
    QObject* obj = sender();
    QTcpSocket* sender = qobject_cast<QTcpSocket*>(obj);
    Q_ASSERT(sender != nullptr);
    if (sender == nullptr) {
        return;
    }
    QByteArray data = m_data + sender->readAll();
    handleHttpRequest(data, sender);
}

void WSHttpServer::handleHttpRequest(const QByteArray &data, QTcpSocket* socket)
{
    qDebug() << Q_FUNC_INFO << data;
    QStringList parts = QString(data).split("\r\n", QString::KeepEmptyParts);
    if (parts.isEmpty()) {
        qWarning() << "Invalid request header";
        qWarning() << data;
        return;
    }

    QStringList parts2 = parts.first().split(" ", QString::KeepEmptyParts);
    if (parts2.count() < 3) {
        qWarning() << "Invalid begin of request header";
        qWarning() << data;
        return;
    }

    int index = parts.indexOf(""); // find first empty row
    if (parts2[0] != "GET" && parts[index+1].isEmpty()) {
        m_data = data; // store data and wait more!
        return;
    }

    WSRequestHandler *handler = getHandler(parts2[0], parts2[1]);
    Q_ASSERT(handler != nullptr);
    handler->run(parts[index+1].toUtf8(), socket);
    m_data.clear(); // handled -> clear cached data
}

WSRequestHandler *WSHttpServer::getHandler(const QString &type, const QString &url)
{
    QString mapperString = QString("%1 %2").arg(type).arg(url);
    WSRequestHandler *handler = m_handlers[mapperString];
    if (handler == nullptr) {
        // url not found -> get default handler for 404 response
        handler = m_handlers[KEY_404];
    }
    return handler;
}
