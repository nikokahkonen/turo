#include <QDebug>
#include <QSqlDriver>
#include <QSqlError>
#include <QSqlQuery>
#include "dabasehandler.h"
#include "wsserverapplication.h"

const QString DRIVER("QSQLITE");

DabaseHandler& DabaseHandler::instance()
{
    static DabaseHandler* handler = nullptr;
    if (handler == nullptr) {
        handler = new DabaseHandler();
    }
    return *handler;
}

void DabaseHandler::open()
{
    if (QSqlDatabase::isDriverAvailable(DRIVER)) {
        m_db = QSqlDatabase::addDatabase(DRIVER);
        QString dbPath = WSServerApplication::databaseFileLocation();
        m_db.setDatabaseName(dbPath + "/wsserver.db");
        if (!m_db.open()) {
            // qFatal exits the application
            qFatal("Failed to open database!");
        }
        // no tables -> create
        QStringList tables = m_db.tables();
        if (tables.isEmpty()) {
            createTables();
        }
    }
}

void DabaseHandler::createTables()
{
    QSqlQuery query("CREATE TABLE users (username TEXT PRIMARY KEY, salt TEXT, hash TEXT)");
    if(!query.isActive()) {
        qWarning() << "ERROR: " << query.lastError().text();
    }
}

void DabaseHandler::setFrequence(int frequence)
{
    m_frequence = frequence;
}

bool DabaseHandler::setCredentials(QString username, QByteArray hash, QByteArray salt)
{
    bool ret = true;
    QSqlQuery query;
    query.prepare("INSERT or REPLACE INTO users(username, salt, hash) VALUES(:username, :salt, :hash)");
    // Use bindValue to avoid SQL injection!
    query.bindValue(":username", username);
    query.bindValue(":salt", QString(salt.toHex(0)));
    query.bindValue(":hash", QString(hash.toHex(0)));
    if (!query.exec()) {
      qWarning() << "ERROR: " << query.lastError().text();
      ret = false;
    }
    return ret;
}

bool DabaseHandler::credentials(QString username, QByteArray& hash, QByteArray& salt)
{
    QSqlQuery query;
    query.prepare("SELECT salt, hash FROM users WHERE username = ?");
    query.addBindValue(username); // using this helps to avoid SQL injection!
    if (!query.exec()) {
        qWarning() << "ERROR: " << query.lastError().text();
        return false;
    }
    if (!query.first()) {
        return false;
    }
    salt = QByteArray::fromHex(query.value("salt").toString().toUtf8());
    hash = QByteArray::fromHex(query.value("hash").toString().toUtf8());
    return true;
}

int DabaseHandler::frequence() const
{
    return m_frequence;
}

DabaseHandler::DabaseHandler()
{
    open();
    m_frequence = 1;
}
