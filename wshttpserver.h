#ifndef WSHTTPSERVER_H
#define WSHTTPSERVER_H

#include <QObject>
#include <QTcpServer>
#include <QQueue>
#include "qsslserver/qsslserver.h"

class WSRequestHandler;

class WSHttpServer : public QObject
{
    Q_OBJECT
public:
    enum class RequestType {
        GET,
        PUT,
        POST
    };

    explicit WSHttpServer(QObject *parent = nullptr);
    void listen(QHostAddress host, quint16 port);
    void addHandler(RequestType type, QString const& url, WSRequestHandler* handler);

private slots:
    void handleNewConnection();
    void readDataFromSocket();
    void handleHttpRequest(QByteArray const& data, QTcpSocket* socket);
    WSRequestHandler *getHandler(QString const& type, QString const& url);
    void onSslErrors(const QList<QSslError> &errors);

private:
    QSslServer m_sslServer;
    QMap<QString, WSRequestHandler*> m_handlers;
    QByteArray m_data;
};

#endif // WSHTTPSERVER_H
