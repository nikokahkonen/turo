#ifndef WSREQUESTHANDLERCONFIG_H
#define WSREQUESTHANDLERCONFIG_H

#include "wsrequesthandler.h"

class WSRequestHandlerConfig : public WSRequestHandler
{
public:
    WSRequestHandlerConfig(QObject *parent = nullptr);
    virtual void run(QByteArray const& data, QTcpSocket* socket);
};

#endif // WSREQUESTHANDLERCONFIG_H
