#ifndef WSREQUESTHANDLERSETPASSWORD_H
#define WSREQUESTHANDLERSETPASSWORD_H

#include "wsrequesthandler.h"

class WSRequestHandlerSetPassword : public WSRequestHandler
{
    Q_OBJECT
public:
    explicit WSRequestHandlerSetPassword(QObject *parent = nullptr);
    virtual void run(QByteArray const& data, QTcpSocket* socket);
};

#endif // WSREQUESTHANDLERSETPASSWORD_H
