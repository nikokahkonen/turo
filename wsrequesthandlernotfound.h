#ifndef WSREQUESTHANDLERNOTFOUND_H
#define WSREQUESTHANDLERNOTFOUND_H

#include "wsrequesthandler.h"

class WSRequestHandlerNotFound : public WSRequestHandler
{
public:
    WSRequestHandlerNotFound(QObject *parent = nullptr);
    virtual void run(QByteArray const& data, QTcpSocket* socket);
};

#endif // WSREQUESTHANDLERNOTFOUND_H
