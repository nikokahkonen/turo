#ifndef SWREQUESTHANDLERWEATHER_H
#define SWREQUESTHANDLERWEATHER_H

#include "wsrequesthandler.h"

class WSRequestHandlerWeather : public WSRequestHandler
{
    Q_OBJECT
public:
    WSRequestHandlerWeather(QObject* parent = nullptr);
    virtual void run(QByteArray const& data, QTcpSocket* socket);

private:
    int m_temperature;
    int m_dewpoint;
};

#endif // SWREQUESTHANDLERWEATHER_H
