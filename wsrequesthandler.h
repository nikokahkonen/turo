#ifndef WSREQUESTHANDLER_H
#define WSREQUESTHANDLER_H

#include <QObject>
#include <QTcpSocket>

class WSRequestHandler : public QObject
{
    Q_OBJECT
public:
    WSRequestHandler(QObject *parent);
    virtual void run(QByteArray const& data, QTcpSocket* socket) = 0;

protected:
    void sendResponse(int responseCode,
                      QByteArray const& data,
                      QTcpSocket* socket);
    QByteArray errorJson(QString errorCode);

private:
    QString responseFromCode(int responseCode);
};

#endif // WSREQUESTHANDLER_H
