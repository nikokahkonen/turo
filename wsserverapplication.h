#ifndef WSSERVERAPPLICATION_H
#define WSSERVERAPPLICATION_H

#include <QCoreApplication>
#include "wshttpserver.h"

class WSServerApplication : public QCoreApplication
{
public:
    WSServerApplication(int argc, char *argv[]);
    void disableCoreDump();
    void startServer(quint16 port);
    static QString databaseFileLocation();
    bool isRoot() const;

private slots:
    void addRoutes();

private:
    WSHttpServer m_restServer;
    bool m_routesAdded;
};

#endif // WSSERVERAPPLICATION_H
