#include <QJsonDocument>
#include <QJsonObject>
#include <QCryptographicHash>
#include <QDateTime>
#include "wsrequesthandlersetpassword.h"
#include "credentialmanager.h"
#include "dabasehandler.h"

WSRequestHandlerSetPassword::WSRequestHandlerSetPassword(QObject *parent)
    : WSRequestHandler(parent)
{
}

void WSRequestHandlerSetPassword::run(const QByteArray &data, QTcpSocket *socket)
{
    QJsonDocument doc = QJsonDocument::fromJson(data);
    QJsonObject root = doc.object();
    QString password = root.value("password").toString();
    if (!CredentialManager::instance().hasAccess(password)) {
        WSRequestHandler::sendResponse(403, QByteArray(), socket);
        return;
    }
    QString newPassword = root.value("new_password").toString();
    if (!CredentialManager::instance().checkPasswordQuality(newPassword)) {
        QByteArray responseData = WSRequestHandler::errorJson("PasswordNotAcceptable");
        WSRequestHandler::sendResponse(500, responseData, socket);
        return;
    }
    if (!CredentialManager::instance().setPassword(newPassword)) {
        QByteArray responseData = WSRequestHandler::errorJson("DatabaseError");
        WSRequestHandler::sendResponse(500, responseData, socket);
        return;
    }
    WSRequestHandler::sendResponse(200, QByteArray(), socket);
}
